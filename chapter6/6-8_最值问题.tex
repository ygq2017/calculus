% !TeX Program = XeLaTeX
\documentclass{LectureSlides}
\title{多元函数最值问题}
\begin{document}
\section{基本概念}
\begin{frame}{最值、最值点}
  \onslide<+->
  设 $f:D\to\R$ 为一\alert{实值函数}，如果存在 $P\in D$ 使得
  \[ f(P)\leq f(x)\qquad x\in D. \]
  则称 $f(P)$ 为函数 $f$ 的\textbf{最小值}，称 $P$ 为函数 $f$ 的\textbf{最小值点}。
  如果存在 $P\in D$ 使得
  \[ f(P)\geq f(x)\qquad x\in D. \]
  则称 $f(P)$ 为函数 $f$ 的\textbf{最大值}，称 $P$ 为函数 $f$ 的\textbf{最大值点}。
  
  \begin{enumerate}[<+->]
    \item 最值点可能不存在，如 $f(x,y)=x^2-y^2$.
    \item 最小值和最大值，如果存在的话，是唯一的。
    \item 最值点可能不止一个，如 $f(x,y)=\sin x\sin y$.
    \item 最小值不大于最大值。
  \end{enumerate}
\end{frame}

\begin{frame}{极值、极值点}
  \onslide<+->
  设 $f:D\to\R$ 为一个实值函数，如果存在 $P\in D$ 以及 $P$ 的邻域 $U(P)$ 使得
  $$ f(P)\leq f(x) \qquad x\in D \cap U(P). $$
  则称 $f(P)$ 为函数 $f$ 的\textbf{极小值}，称 $P$ 为函数 $f$ 的\textbf{极小值点}。
  如果存在 $P\in D$ 以及 $P$ 的邻域 $U(P)$ 使得
  $$ f(P)\geq f(x) \qquad x\in D \cap U(P). $$
  则称 $f(P)$ 为函数 $f$ 的\textbf{极大值}，称 $P$ 为函数 $f$ 的\textbf{极大值点}。
  
  \begin{enumerate}[<+->]
    \item 极值点可能不存在，如 $f(x,y)=x^2-y^2$.
    \item 极值可能不止一个，如 $f(x,y)=(x^2+y^2)\sin(xy)$.
    \item 对应于同一个极值的极值点可能不止一个。
    \item 极小值可能大于极大值.
  \end{enumerate}
\end{frame}

\section{内部极值点}
\subsection{必要条件}
\begin{frame}{理论基础}
  \onslide<+->
  \begin{theorem}[费马引理]
    设函数 $f$ 在点 $a$ 的某个邻域 $(a-\delta, a+\delta)$ 内有定义, 
    如果 $a$ 是 $f$ 的一个极值点，且 $f$ 在 $a$ 点处可导，则 $f'(a)=0$.
  \end{theorem}

  \onslide<+->
  \begin{ctikzpicture}
    \fill[visibleon=<.->,gray!60] (0,0) circle[radius=1.5];    
    \draw[visibleon=<.->,vector] (3,0)--(5,0);
    \path[visibleon=<.->](60:1.6) edge[bend left, vector] node[midway, above]{$f$} (4,0.1);
    
    \draw[visibleon=<+->,vector] (-5,0)--(-3,0);
    \fill[visibleon=<.->](-4,0) circle[radius=2pt] node[below] {$0$};
    
    \fill[visibleon=<+->,c1](0,0) circle[radius=2pt];
    \draw[visibleon=<+->,c1, thick](30:1) to[curve through={(0,0)}] (250:1);
    \path[visibleon=<+->,c1](-3.8,-0.1) edge [bend right,vector] node[midway, below] {$g$} (-0.2,0);
    
    \fill[visibleon=<+->,c2](135:1.5) circle[radius=2pt];
    \draw[visibleon=<+->,c2, thick](90:1) to[curve through={(135:1.5)}] (180:0.8); 
    \path[visibleon=<+->,c2](-4,0.1) edge [bend left,vector] node[midway, above] {$g$} (135:1.6);

    \path[visibleon=<+->](-3.8,-0.2) edge[bend right=55, vector] node[midway, below]{$f(g(t))$} (4,-0.1);
  \end{ctikzpicture}
\end{frame}

\begin{frame}{极值点的必要条件}
  \onslide<+->
  \begin{theorem}[多元函数极值点的必要条件]
    设函数 $f\colon M\to\R$, $g:(-\delta, \delta)\to M$, 其中 $\delta>0$.
    若 $g(0)$ 是函数 $f$ 的极值点且 $f(g(t))$ 在 $t=0$ 处可导, 
    则此导数为 $0$.
  \end{theorem}

  \onslide<+->
  \begin{theorem}[特殊情况]
    设 $f\colon M\to\R$, 
    如果 $P$ 是 $f$ 的一个极值点且 $f$ 点 $P$ 处 $f$ 的某个偏导数存在，
    则此偏导数一定为 $0$.
  \end{theorem}

  \onslide<+->
  \begin{remark}
    定理不需要函数的两个偏导数都存在，
    只需要一个偏导数存在且此偏导数不为 $0$ 就能说明此点一定不是极值点。
  \end{remark}

  \onslide<+->
  称函数两个偏导数都存在且同时为 $0$ 的点为函数的\textbf{驻点}。
\end{frame}


\subsection{充分条件}
\begin{frame}{内部极值点的充分条件}
  \onslide<+->
  \begin{theorem}[二元函数内部极值点的充分条件]
    设函数 $f(x,y)$ 在点 $P(a,b)$ 的某个邻域内二阶连续可微，
    \onslide<+-> 且 $f_x(a,b)=f_y(a,b)=0$, 
    \onslide<+->
    记 $H=\begin{vmatrix}f_{xx}(a,b)&f_{xy}(a,b)\\ f_{yx}(a,b)&f_{yy}(a,b)\end{vmatrix}$,
    则: 
    \begin{itemize}[<+->]
      \item 若 $H>0$, 则 $P$ 为 $f$ 的极值点，且
        \begin{itemize}[<+->]
          \item 当 $f_{xx}(a,b)>0$ 时 $P$ 为极小值点
          \item 当 $f_{xx}(a,b)<0$ 时 $P$ 为极大值点
        \end{itemize}
      \item 若 $H<0$, 则 $P$ 不是 $f$ 的极值点。
    \end{itemize}
  \end{theorem}
  \onslide<+->
  \begin{remark}
    $H>0$ 时， $f_{xx}(a,b)$ 与 $f_{yy}(a,b)$ 同号。
  \end{remark}
  \onslide<+->
  \begin{remark}
    对于一般的多元函数，并不是用 $H$ 的正负来判断的，
    而是用类似的方阵的正定、负定性来判断的。
  \end{remark}
\end{frame}

\begin{frame}{定理中的函数是什么样子}
  \onslide<+->
  当 $H\neq0$ 时，根据定理，可知
  \begin{enumerate}[<+->]
    \item 点 $(0,0)$ 是 $f(x,y)=x^2+y^2$ 的\onslide<+->{极小值点}
    \item 点 $(0,0)$ 是 $f(x,y)=-x^2-y^2$ 的\onslide<+->{极大值点}
    \item 点 $(0,0)$ \only<+->{不}是 $f(x,y)=x^2-y^2$ 的\onslide<.->{极值点}
  \end{enumerate}
  
  \onslide<+->
  这虽然是三个特殊的例子，但它们也代表了一般的情况。
  
  \onslide<+->
  利用 Morse 引理可知满足定理条件的函数在对应点处的
  某个邻域内可以通过坐标变换的方式写成这三种形式。
\end{frame}

\begin{frame}{不能判定的情况}
  \onslide<+->
  当 $H=0$ 时，所有的可能都有，如
  \begin{enumerate}[<+->]
    \item 点 $(0,0)$ 是 $f(x,y)=x^2$ 的\onslide<+->{极小值点}
    \item 点 $(0,0)$ 是 $f(x,y)=-x^2$ 的\onslide<+->{极大值点}
    \item 点 $(0,0)$ 是 $f(x,y)=x^2+y^4$ 的\onslide<+->{极小值点}    
    \item 点 $(0,0)$ 是 $f(x,y)=-x^2-y^4$ 的\onslide<+->{极大值点}
    \item 点 $(0,0)$ \only<+->{不}是 $f(x,y)=x^2-y^4$ 的\onslide<.->{极值点}
    \item 点 $(0,0)$ 是 $f(x,y)=x^4+y^4$ 的\onslide<+->{极小值点}
    \item 点 $(0,0)$ 是 $f(x,y)=-x^4-y^4$ 的\onslide<+->{极大值点}
    \item 点 $(0,0)$ \only<+->{不}是 $f(x,y)=x^4-y^4$ 的\onslide<.->{极值点}
  \end{enumerate}
\end{frame}

\begin{frame}{开集上的极值问题举例}
  \small
  \onslide<+->
  \begin{question}
    求函数 $f(x,y)=x^3-y^3+3x^2+3y^2-9x$ 的极值与极值点。
  \end{question}
  \onslide<+->
  \begin{solution}
    容易看出 $f$ 在 $\R^2$ 上二阶连续可微函数，
    \onslide<+-> 计算可得
    \begin{align*}
      f_x &=3(x-1)(x+3) & \onslide<+->{f_{xx}&=6x+6 & \onslide<+->{f_{xy}&=0     \\}}
      f_y &=-3y(y-2)    & \onslide<.(-1)->{f_{yx}&=0& \onslide<.->{f_{yy}&=-6y+6}}
    \end{align*}
    \onslide<+-> 解 $f_x=f_y=0$ 得驻点
    \onslide<+-> $(1,0)$, $(1,2)$, $(-3,0)$, $(-3,2)$, 
    \onslide<+-> 从而
    \begin{tabu}{*2{X[$c$]}*3{X[0.6,$c$]}X[2,$c$]X[2,c]}\toprule
      (x,y)  & f(x,y) & f_{xx} & f_{yy} & f_{xy} & f_{xx}f_{yy} - f_{xy}^2 & \textbf{结论} \\\midrule
      (1,0)  & \only<+(+5)->{-5} & \only<+(-1)->{12 } & \only<+(-1)->{6 } & \only<+(-1)->{0} & \only<+(-1)->{+} & \only<+(-1)->{是极小值点} \\
      (1,2)  &                   & \only<.(-5)->{12 } & \only<.(-4)->{-6} & \only<.(-3)->{0} & \only<.(-2)->{-} & \only<.(-1)->{不是极值点} \\
      (-3,0) &                   & \only<.(-5)->{-12} & \only<.(-4)->{6 } & \only<.(-3)->{0} & \only<.(-2)->{-} & \only<.(-1)->{不是极值点} \\
      (-3,2) & \only<.->{31}     & \only<.(-5)->{-12} & \only<.(-4)->{-6} & \only<.(-3)->{0} & \only<.(-2)->{+} & \only<.(-1)->{是极大值点} \\\bottomrule
    \end{tabu}
    \onslide<+->
    所以函数 $f$ 的极小值为 $-5$, 其所对应的极小值点为 $(1,0)$, 
    极大值为 $31$, 其所对应的极大值点为 $(-3,2)$.
  \end{solution}
\end{frame}

\subsection{开集上的极值问题}
\begin{frame}{开集上的极值问题求解步骤}
  \begin{enumerate}[<+->]
    \item 确认函数是二阶连续可微的，以保证可以利用内部极值点的必要条件和充分条件。
    \item 利用内部极值点的必要条件，去除函数不能是极值点的点，得到可能是极值点的点。
    \item 利用内部极值点的充分条件，判断上一步得到的点是否为极值点以及是什么类型的极值点。
    \item 对于不能利用内部极值点的充分条件判断的点，利用其它方法判断。
    \item 总结并回答问题。
  \end{enumerate}
\end{frame}


\begin{frame}{极值问题练习}\small
  \onslide<+->
  \begin{question}
    求函数 $f(x,y)=4xy-x^4-y^4$ 的极值与极值点。
  \end{question}
  \onslide<+->
  \begin{solution}
    容易看出 $f$ 在 $\R^2$ 上二阶连续可微函数，
    \onslide<+-> 计算可得
    \begin{align*}
      f_x &=4y-4x^3 & \onslide<+->{f_{xx}&=-12x^2 & \onslide<+->{f_{xy}&=4   \\}}
      f_y &=4x-4y^3 & \onslide<.(-1)->{f_{yx}&=4  & \onslide<.->{f_{yy}&=-12y^2}}
    \end{align*}
    \onslide<+-> 解 $f_x=f_y=0$ 得驻点
    \onslide<+-> $(0,0)$, $(1,1)$, $(-1,-1)$, 
    \onslide<+-> 从而
    \begin{tabu}{X[1.4,$c$]X[1.2,$c$]*2{X[0.7,$c$]}X[0.6,$c$]X[2.2,$c$]X[2.2,c]}\toprule
      (x,y)  & f(x,y) & f_{xx} & f_{yy} & f_{xy} & f_{xx}f_{yy} - f_{xy}^2 & \textbf{结论} \\\midrule
      (0,0)   & \only<+(+5)->{} & \only<+(-1)->{0  } & \only<+(-1)->{0  } & \only<+(-1)->{4} & \only<+(-1)->{-} & \only<+(-1)->{不是极值点} \\
      (1,1)   & \only<.->{2}    & \only<.(-5)->{-12} & \only<.(-4)->{-12} & \only<.(-3)->{4} & \only<.(-2)->{+} & \only<.(-1)->{是极大值点} \\
      (-1,-1) & \only<.->{2}    & \only<.(-5)->{-12} & \only<.(-4)->{-12} & \only<.(-3)->{4} & \only<.(-2)->{+} & \only<.(-1)->{是极大值点} \\\bottomrule
    \end{tabu}
    \onslide<+->
    所以函数 $f(x,y)$ 的极大值为 $2$, 其所对应的极小值点为 $(1,1)$ 和 $(-1,-1)$, 
    \onslide<+->
    没有极小值。
  \end{solution}
\end{frame}


\section{条件极值点}
\begin{frame}{极值问题中的条件}
  \onslide<+->
  实际应用中，很少直接求函数 $f(x,y,z)$ 的最值或极值，
  往往会对函数的定义域有一定的限制，
  \onslide<+-> 限制条件主要有三类：
  \begin{enumerate}[<+->]
    \item[(1)] 严格不等式 $F(x,y,z)>0$ 或 $F(x,y,z)<0$.
    \item[(2)] 等式 $F(x,y,z)=0$.
    \item[(3)] 非严格不等式 $F(x,y,z)\geq0$ 或 $F(x,y,z)\leq0$.
  \end{enumerate}
  \onslide<+->
  \begin{remark}
    当 $F$ 连续时，条件(2)表示一个三维开集。
  \end{remark}
  \onslide<+->
  \begin{remark}
    当 $F$ 连续可微且 $\grad F\neq\vecw0$ 时，
    条件(2)若不是空集，则表示一个二维无边曲面。
  \end{remark}
  \onslide<+->
  \begin{remark}
    当 $F$ 连续可微且 $\grad F\neq\vecw0$ 时，
    条件(3)若不是空集，则表示三维带边曲面。
    它的内部是一个三维开集，边界是一个二维无边曲面。
  \end{remark}  
\end{frame}

\begin{frame}{一个简单的例子}
  \onslide<+->
  \begin{question}
    求 $S=xy+xz+yz$ 在条件 $xyz=1$ 下的极值和极值点。
  \end{question}
  \onslide<+->
  \begin{solution}
    由 $xyz=1$ 可知 $z=\frac1{xy}$, 
    \onslide<+-> 从而
    \[ S = xy+xz+yz = xy+\frac1x+\frac1y \]
    \onslide<+->
    计算可得
    \begin{align*}
      S_x&=y-\frac1{x^2}  &  \onslide<+->{S_{xx}&=\frac2{x^3}  &  \onslide<+->{S_{xy}&=1         \\}}
      S_y&=x-\frac1{y^2}  &  \onslide<.(-1)->{S_{yx}&=1        &  \onslide<.->{S_{yy}&=\frac2{y^3} }}
    \end{align*}
    \onslide<+->
    解 $S_x=S_y=0$ 可得驻点 $(1,1)$, 
    \onslide<+->
    由此点处 $S_{xx}S_{yy}-S_{xy}^2=3>0$, $S_{xx}=2>0$, 可知此点为极小值点。
    \onslide<+->
    所以 $S$ 有极小值 $3$, 对应的极小值点 $(1,1,1)$, 
    \onslide<+-> 无极大值。
  \end{solution}
\end{frame}

\begin{frame}{问题与思路}
  \onslide<+->
  求可微函数 $f(x,y,z)$ 在等式条件 $F(x,y,z)=0$ 下的极值，
  其中 $F(x,y,z)$ 连续可微且当 $F(x,y,z)=0$ 时 $\grad F \neq\vecw0$.
  \begin{enumerate}[<+->]
    \item 无条件时，在极值点处有 $\grad f=\vecw0$ 是因为否则的话，
      沿方向 $\grad f$ 会有函数值比此点的大，
      沿反方向 $-\grad f$ 会有函数值比此点的小，
      从而导致此点不是极值点。
    \item 问题中的条件 $F(x,y,z)=0$ 表示一个二维无边曲面，
      此曲面上过点 $P$ 的曲线的切方向并不是任意的，
      而是要满足与向量 $\grad F$ 垂直才行。
    \item 在问题中的极值点处，
      $\grad f$ 与曲面 $F(x,y,z)=0$ 上所有曲线的切方向都垂直，
      即 $\grad f$ 平行于 $\grad F$, 又由已知条件 $\grad F\neq\vecw0$, 
      即存在 $\lambda$ 使得 $\grad f = \lambda \grad F$.
  \end{enumerate}
\end{frame}


\subsection{等式条件上内部极值点的必要条件}
\begin{frame}{拉格朗日乘子法}
  \onslide<+->
  \begin{theorem}
	  设函数 $f(x,y,z)$, $F(x,y,z)$ 都可微，
	  \onslide<+->
	  作\textbf{拉格朗日函数}
	  \[ L(x,y,z,\lambda) = f(x,y,z) - \lambda F(x,y,z) \]
	  \onslide<+->
	  如果点 $(a,b,c)$ 是函数 $f(x,y,z)$ 在条件 $F(x,y,z)=0$ 下的内部极值点，
	  \onslide<+->
	  则存在 $d$ 使得在点 $(a,b,c,d)$ 处 $\grad L=\vecw0$.
  \end{theorem}
  \onslide<+->
  不难看出
  \[
    \grad L = \vecw0 
    \onslide<+-> \Iff
    \begin{dcases}
      \grad f = \lambda \grad F \\
      F(x,y,z) = 0
    \end{dcases}
  \]
  \onslide<+->
  \begin{remark}
    此定理给出了等式条件上内部极值点的\alert{必要条件}，从上式可以看出，
    在极值点处一定有 $\grad f$ 与 $\grad F$ 平行。
  \end{remark}
\end{frame}

\begin{frame}{条件最值举例}\small
  \onslide<+->
  \begin{question}
    求表面积为 $12$ \si{m^2} 的无盖长方体的最大体积。
  \end{question}
  \onslide<+->
  \begin{solution}
    设长方体的长宽高分别为 $x$ \si{m}, $y$ \si{m}, $z$ \si{m},
    \onslide<+->
    长方体的表面积为 $S$ \si{m^2}, 长方体的体积为 $V$ \si{m^3}.
    \onslide<+->
    则 $S=xy + 2xz + 2yz=12$, $V=xyz$, $x>0$, $y>0$, $z>0$.
    \onslide<+->
    原问题可等价为求函数 $V=xyz$ 在条件 $S=12$ 下的最大值。
    \onslide<+-> 做拉格朗日函数
    \[ L(x,y,z,\lambda) = xyz - \lambda(xy + 2xz + 2yz - 12) \]
    \onslide<+->
    计算可得
    \begin{align*}
      L_x &= yz - \lambda(y + 2z) & L_z &= xy - \lambda(2x+2y)      \\
      L_y &= xz - \lambda(x + 2z) & L_\lambda &= xy + 2xz + 2yz - 12
    \end{align*}
    \onslide<+->
    解 $L_x=L_y=L_z=L_\lambda=0$ 可得唯一驻点 $(2,2,1,2/3)$.
    \onslide<+->
    由实际问题可知点 $(2,2,1)$ 就是 $V$ 在 $S=12$ 时的最大值点，
    \onslide<+->
    此时 $V=4$, 即表面积为 $12$ \si{m^2} 的无盖长方体的最大体积为 $4$ \si{m^3}.
  \end{solution}
\end{frame}


\subsection{三元函数两个限制条件}
\begin{frame}{拉格朗日乘子法}
  \onslide<+->
  \begin{theorem}
	  设三元函数 $f$, $F$, $G$ 都可导，作\textbf{拉格朗日函数}
	  \[ L(x,y,z,\lambda_1,\lambda_2) = f(x,y,z) - \lambda_1 F(x,y,z) - \lambda_2 G(x,y,z) \]
	  如果点 $(a,b,c)$ 是函数 $f(x,y,z)$ 在条件 $F(x,y,z)=0$ 和 $G(x,y,z)=0$ 下的内部极值点，
	  则存在 $d$, $e$ 使得在点 $(a,b,c,d,e)$ 处 $\grad L = \vecw0$.
  \end{theorem}
  \onslide<+->
  不难看出
  \[
    \grad L = \vecw0 
    \onslide<+-> \Iff
    \begin{dcases}
      \grad f = \lambda_1 \grad F + \lambda_2 \grad G \\
      F(x,y,z) = 0\\
      G(x,y,z) = 0
    \end{dcases}
  \]
\end{frame}


\section{最值问题}
\begin{frame}{最值问题求解思路}
  \onslide<+->
  \alert<+->{有界闭区域上的连续函数}一定有最大值和最小值，
  下面我们来研究如何计算它的最值和最值点。
  \begin{enumerate}[<+->]
    \item 利用内部极值点的必要条件求函数 $f$ 内部的可能极值点。
    \item 利用拉格朗日乘子法求 $f$ 边界上的可能极值点。
    \item 比较各个可能极值点处的值，由最值的存在性求得函数的最值和最值点。
  \end{enumerate}
\end{frame}

\begin{frame}{最值问题举例}
  \onslide<+->
  \begin{question}
    求函数 $f(x,y,z)=x+y+z$ 在球面 $x^2+y^2+z^2=3$ 上的最值与最值点。
  \end{question}
  \onslide<+->
  \begin{solution}
    做拉格朗日函数
    \[ L(x,y,z,\lambda)=x+y+z - \lambda(x^2+y^2+z^2-3) \]
    \onslide<+->
    从而
    \begin{align*}
      L_x &= 1-2\lambda x  &  L_z &= 1-2\lambda z  \\
      L_y &= 1-2\lambda y  &  L_\lambda &= x^2+y^2+z^2-3
    \end{align*}
    \onslide<+->
    解 $L_x=L_y=L_z=L_\lambda=0$ 可得驻点 $\pm(1,1,1,1/2)$, 
    \onslide<+->
    因为球面 $x^2+y^2+z^2=3$ 是有界闭集，所有 $f$ 在其上一定由最大值和最小值，
    \onslide<+-> 比较 $f(1,1,1)=3$ 和 $f(-1,-1,-1)=-3$ 
    \onslide<+-> 可得 $f$ 的最大值为 $3$, 最小值为 $-3$.
  \end{solution}  
\end{frame}

\begin{frame}{最值问题举例}\small
  \onslide<+->
  \begin{question}
    求函数 $f(x,y) = 2x^2+3y^2-4x+2$ 在有界闭区域 $D=\Set[\big]{(x,y) \given x^2+y^2\leq16}$ 
    上的最值与最值点。
  \end{question}
  \onslide<+->
  \begin{solution}
    计算得 $f_x=4x-4$, $f_y=6y$,
    \onslide<+->
    解 $f_x=f_y=0$ 可得内部可能极值点 $(1,0)$.
    \onslide<+->
    做拉格朗日函数 $L(x,y,\lambda)= f(x,y)-\lambda(x^2+y^2-16)$, 
    \onslide<+-> 计算得
    \begin{align*}
      L_x &= 4x-4 - 2\lambda x  &
      L_y &= 6y - 2\lambda y    &
      L_\lambda &= x^2+y^2-16
    \end{align*}
    \onslide<+->
    解 $L_x=L_y=L_\lambda=0$ 得 $f$ 可能的极值点
    \onslide<+->
    \begin{align*}
      &(-4,0)  &&(4,0)  &&(-2,-2\sqrt3)  &&(-2,2\sqrt3)
    \end{align*}
    \onslide<+->
    比较函数 $f$ 在这五个点处的值
    \onslide<+->
    \begin{align*}
      f(1,0)&=0  &  f(-4,0)&=50  &  f(4,0)&=18  \\
         && f(-2,-2\sqrt3)&=54  &  f(-2,2\sqrt3)&=54
    \end{align*}
    \onslide<+->
    可知函数的最大值为 $54$, 最大值点为 $(-2,-2\sqrt3)$ 和 $(-2,2\sqrt3)$,
    函数的最小值为 $0$, 最小值点为 $(1,0)$.
  \end{solution}  
\end{frame}
\end{document}
