% !TeX Program = XeLaTeX
\documentclass{LectureSlides}
\title{全微分}
\begin{document}

\section{微分的概念}
\begin{frame}{微分}
  \onslide<+->
  当函数 $z=f(x,y)$ 在点 $(a,b)$ 处分别获得增量 $\iix$, $\iiy$ 时，
  \onslide<+->
  对应函数值的增量 
  \[ \iiz = f(a+\iix,b+\iiy) - f(a,b) \]
  \onslide<+->
  称为函数 $z=f(x,y)$ 在点 $(a,b)$ 处的\textbf{全增量}。

  \onslide<+->
  \begin{definition}[微分]
    若存在常数 $A$ 和 $B$ 使得：
    \[ \iiz = A\iix + B\iiy + \varepsilon_1 \iix + \varepsilon_2 \iiy, \]
    其中当 $(\iix, \iiy) \to (0,0)$ 时 $\varepsilon_1\to0$, $\varepsilon_2\to0$.
    \onslide<+->
    则称函数 $f(x,y)$ 在点 $(a,b)$ 处\textbf{可微}，
    \onslide<+->
    并称关于 $\iix$, $\iiy$ 的线性函数 $A\iix + B\iiy$ 
    为函数 $f(x,y)$ 在点 $(a,b)$ 处的\textbf{微分}，
    \onslide<+->
    记为：
    \[ \df\big|_{(a,b)} = A\iix + B\iiy. \]
  \end{definition}
\end{frame}

\subsection{微分的意义}
\begin{frame}{微分的意义}
  \onslide<+->
  函数 $f(x,y)$ 在点 $(a,b)$ 处可微，则
  \begin{align*}
    \iif 
    &= f(a+\iix,b+\iiy) - f(a,b) \\
    &= \underbracket[1pt][5pt]{A\iix + B\iiy}_{\text{线性主部}} + 
       \underbracket[1pt][5pt]{\varepsilon_1 \iix + \varepsilon_2 \iiy}_{\text{高阶无穷小}} \\
    \onslide<+->{&\approx A\iix + B\iiy}
  \end{align*}
  \onslide<+->
  若代入 $\iix=x-a$, $\iiy=y-b$, 则有
  \onslide<+->
  \[ f(x,y) - f(a,b) \approx A(x-a) + B(y-b) \]
  \onslide<+->
  称为函数 $f(x,y)$ 在点 $(a,b)$ 处的\textbf{线性近似}。

  \onslide<+->
  几何上表示曲面 $z=f(x,y)$ 在点 $(a,b)$ 处的切平面方程为
  \[ z - f(a,b) = A(x-a) + B(y-b) \]
\end{frame}

\section{微分的计算}
\begin{frame}{用定义求微分}
  \onslide<+->
  \begin{question}
    求函数 $z=Ax+By+C$ 的全微分？
  \end{question}
  \onslide<+->
  \begin{solution}
    由 $\iiz = A\iix + B\iiy$ 可知，
    \onslide<+->
    函数 $z=Ax+By+C$ 在其定义域上所有点处都可微，而且 
    \[ \dz = A\iix + B\iiy. \]
  \end{solution}
  
  \onslide<+->
  当 $A=1$, $B=0$, $C=0$ 时，$z=x$，
  \onslide<+->
  则有 
  \[ \dx=\dz=\iix \]
  \onslide<+->
  同理可得 $\dy=\iiy$.

  \onslide<+->
  一般有：\alert{自变量的微分等于它的增量}。
  \onslide<+->
  从而通常把函数在一点的全微分\alert{对称}地写成下面的形式：
  \[ \dz = A\dx + B\dy. \]
\end{frame}

\subsection{微分与偏导的关系}
\begin{frame}{全微分中的系数}
  \onslide<+->
  若函数 $z=f(x,y)$ 在点 $(a,b)$ 处可微，则
  \begin{align*}
    \iiz 
    &= f(a+\iix,b+\iiy) - f(a,b) \\
    &= A\iix + B\iiy + \varepsilon_1 \iix + \varepsilon_2 \iiy, 
  \end{align*}
  其中当 $(\iix, \iiy) \to (0,0)$ 时 $\varepsilon_1\to0$, $\varepsilon_2\to0$.

  \onslide<+->
  若 $\iiy=0$ 时，上式即为
  \onslide<+->
  \begin{align*}
    \iiz 
    &= f(a+\iix,b) - f(a,b) \\
    &= A\iix + \varepsilon_1 \iix \quad \iix\to0 
  \end{align*}
  \onslide<+->
  从而 
  \[ A = \left.\ddx{f(x,b)}\right|_{x=a} = \pdx{f}(a,b). \]
  \onslide<+->
  同理可得
  \[ B = \left.\ddy{f(a,y)}\right|_{y=b} = \pdy{f}(a,b). \]
\end{frame}
  
\begin{frame}{微分与偏导数的关系}
  \onslide<+->
  \vspace*{1ex}
  \begin{theorem}[微分与偏导数的关系]
    如果函数 $z=f(x,y)$ 在点 $(a,b)$ 处可微，则：
    \[ 
      \df|_{(a,b)} = A\dx + B\dy 
      \Iff \pdx{f}(a,b) = A, \pdy{f}(a,b) = B.
    \]
  \end{theorem}

  \onslide<+->
  \vspace*{-1ex}
  由定理可知，若函数 $f(x,y)$ 可微，则
  \[ \df = \pdx{f}\dx + \pdy{f}\dy \]

  \onslide<+->
  \begin{ctikzpicture}
    \fill[c1!40, opacity=0.4] (-1.6,0) ellipse (3 and 1.2);
    \fill[c2!40, opacity=0.4] (1.6,0) ellipse (3 and 1.2);
    \fill[c3!40, opacity=0.4] (0,0) circle (0.6);
    \draw[c1, thick] (-1.6,0) ellipse (3 and 1.2) ++(-1,0) node {\textbf{连续}};
    \draw[c2, thick] (1.6,0) ellipse (3 and 1.2) ++(1,0) node {\textbf{可导}};
    \draw[c3, thick] (0,0) circle (0.6) node {\textbf{可微}};
  \end{ctikzpicture}
\end{frame}

\begin{frame}{考察函数在一点处的连续、可导、可微性质}
  \onslide<+->
  \begin{question}
    考察函数 $f$ 在点 $(0,0)$ 处的可微性和可导性。
    \[
      f(x,y) = 
      \begin{dcases}
        \frac{xy}{\sqrt{x^2+y^2}},& x^2+y^2\neq0;\\
        0,& x^2+y^2=0.
      \end{dcases}
    \]
  \end{question}
  \onslide<+->
  \begin{solution}
    不连续、可导、不可微。
  \end{solution}

  \onslide<+->
  \begin{question}
    考察函数 $f$ 在点 $(0,0)$ 处的连续性、可导性、可微性。
    \[
      f(x,y) =
      \begin{dcases}
        \frac{x^2y}{x^2+y^2},& x^2+y^2\neq0;\\
        0,& x^2+y^2=0.
      \end{dcases}
    \]
  \end{question}
  \onslide<+->
  \begin{solution}
    连续、可导、不可微
  \end{solution}
\end{frame}

\subsection{可微的充分条件}
\begin{frame}{可微的充分条件}
  \onslide<+->
  \begin{theorem}[可微的充分条件]
    若函数 $f(x,y)$ 在点 $(a,b)$ 的\alert{某个邻域内可偏导}，
    且偏导数在点 $(a,b)$ 处连续，则函数 $f(x,y)$ 在点 $(a,b)$ 处可微。
  \end{theorem}

  \onslide<+->
  类似于二元函数，可以定义三元函数和一般多元函数的全微分，
  如果三元函数 $f(x,y,z)$ 在开区域内连续可偏导，
  则在此区域可微且
  \[ \df = \pdx{f}\dx + \pdy{f}\dy + \pdz{f}\dz. \]
\end{frame}

\begin{frame}{微分的运算法则}
  \begin{itemize}
    \item $\d*(f+g) = \df + \dg$,
    \item $\d*(f-g) = \df - \dg$,
    \item $\d*(f\cdot g) = \df\cdot g + f\cdot\dg$,
    \item $\d*\frac fg = \frac{\df\cdot g - f\cdot\dg}{g^2}$,
    \item $\d*(f\circ g) = \df\circ g \cdot \dg$.
  \end{itemize}
\end{frame}

\begin{frame}{微分计算例题} \small
  \onslide<+->
  \begin{question}
    求函数 $f(x,y)=x\E^{xy}$ 的全微分。
  \end{question}
  \onslide<+->
  \begin{solution}[name=方法一]
    求两个一阶偏导数可得
    \onslide<+->
    \begin{align*}
      f_x &= \E^{xy} + xy\E^{xy} &
      f_y &= x^2\E^{xy}
    \end{align*}
    \onslide<+->
    显然它们连续，从而函数 $f$ 可微且
    \onslide<+->
    \begin{align*}
      \df 
      &= f_x\dx + f_y\dy \\
      \onslide<+->{&= \E^{xy}\big((1+xy)\dx + x^2\dy\big). }
    \end{align*}
  \end{solution}
  \vspace*{-1ex}
  \onslide<+->
  \begin{solution}[name=方法二]
    由微分的计算公式可得：
    \begin{align*}
      \df 
      &= f_x\dx + f_y\dy \\
      \onslide<+->{&= \big(\E^{xy} + xy\E^{xy}\big)\dx + x^2\E^{xy}\dy }
    \end{align*}
  \end{solution}
  \vspace*{-1ex}
  \onslide<+->
  \begin{solution}[name=方法三]
    由微分的运算规律可得：
    \begin{align*}
      \df 
      &= \E^{xy}\dx + x\E^{xy}\d*(xy)  \\
      \onslide<+->{&= \E^{xy}\dx + x\E^{xy}(y\dx + x\dy) }
    \end{align*}
  \end{solution}
\end{frame}


\begin{frame}{全微分计算练习}
  \onslide<+->
  \begin{question}
    计算函数 $f(x,y,z)=x^{yz}$ 的全微分。
  \end{question}
  \onslide<+->
  \begin{solution}\vspace*{-1.5em}
    \begin{align*}
      \df(x,y,z) 
      &= \d*\big(x^{yz}\big) \\
      \onslide<+->{&= yz x^{yz-1} \dx + x^{yz}\ln x \d*(yz) \\}
      \onslide<+->{&= yz x^{yz-1} \dx + x^{yz}\ln x \big(z\dy + y\dz\big)}
    \end{align*}
  \end{solution}

  \onslide<+->
  \begin{question}
    求函数 $z=\ln(x^2+y^2+1)$ 在点 $(1,2)$ 处, 当 $\iix=0.1$, $\iiy=-0.1$ 时的全微分。
  \end{question}
  \onslide<+->
  \begin{solution}
    计算微分可得
    \onslide<+->
    \[ \dz = \frac{2x\dx+2y\dy}{x^2 + y^2 + 1} \]
    \onslide<+->
    当 $(x,y)=(1,2)$, $\dx=\iix=0.1$, $\dy=\iiy=-0.1$ 时
    \onslide<+->
    \[ \dz = \frac{2\times1\times0.1 + 2\times2\times(-0.1)}{1^2 + 2^2 + 1} = -\frac1{30}. \]
  \end{solution}
\end{frame}


\section{微分的叠加性}
\begin{frame}{微分的叠加性}
  \onslide<+->
  若函数 $z=f(x,y)$ 可微，则 
  \begin{align*}
    \dz 
    &= \pdx{f}\dx + \pdy{f}\dy \\
    \onslide<+->{&= \d\big(f(x,c)\big)\big|_{c=y} + \d\big(f(c,y)\big)\big|_{c=x}}
  \end{align*}
  \onslide<+->
  微分 $\dz$ 可以看成只有 $x$ 变化引起的部分 $\pdx{f}\dx$ 
  与只有 $y$ 变化引起的部分 $\pdy{f}\dy$ 之和。
  \onslide<+->
  此性质称为\textbf{微分的叠加性}。

  \onslide<+->
  \begin{question}
    用微分的叠加性求微分 $\d*(x^x)$.
  \end{question}
  \onslide<+->
  \begin{solution}\vspace*{-1.5em}
    \begin{align*}
      \d*(x^x)
      \onslide<+->{&= \d\big(x^c\big)\big|_{c=x} + \d\big(c^x\big)\big|_{c=x} \\}
      \onslide<+->{&= \big(c x^{c-1}\dx\big)\big|_{c=x} + \big(c^x\ln c\big)\big|_{c=x} \\}
      \onslide<+->{&= x^x \dx + x^x \ln x \dx \\}
      \onslide<+->{&= (1+\ln x) x^x \dx }
    \end{align*}
  \end{solution}
\end{frame}
\end{document}
