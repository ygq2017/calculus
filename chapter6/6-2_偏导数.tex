% !TeX Program = XeLaTeX
\documentclass{LectureSlides}
\title{偏导数}
\begin{document}
\section{一阶偏导}

\begin{frame}{从导数到偏导数}
  \onslide<+->
  设函数 $f(x,y)$ 在点 $(a,b)$ 的某个邻域内有定义，
  \onslide<+-> 记
  \begin{align*}
    g(x)&=f(x,b) & h(y)&=f(a,y)
  \end{align*}
  \onslide<+->
  对这两个一元函数求导可得：
  \onslide<+->
  \begin{align*} 
    \onslide<+->{
      g'(x) &= \lim_{s\to0}\frac{g(x+s)-g(x)}s 
      \onslide<+->{ = \lim_{s\to0}\frac{g(x+s,b)-g(x,b)}s.}\\
    }    
    \onslide<+->{
      h'(y) &= \lim_{s\to0}\frac{h(y+s)-h(y)}s 
      \onslide<+->{ = \lim_{s\to0}\frac{h(a,y+s)-h(a,y)}s.}
    }
  \end{align*}
  \onslide<+->
  特殊地:
  \begin{align*}
    g'(a) &= \lim_{s\to0}\frac{f(a+s,b)-f(a,b)}s \\
    h'(b) &= \lim_{s\to0}\frac{g(a,b+s)-g(a,b)}s
  \end{align*}
  \onslide<+->
  表示在点 $(a,b)$ 处函数值 $f(x,y)$ 相对于变量 $x$ 或 $y$ 的变化率。
\end{frame}

\begin{frame}{偏导数}
  \onslide<+->
  \begin{definition}[偏导数]
    设函数 $f$ 在点 $(a,b)$ 的某个邻域内有定义，如果极限
    \begin{equation}\label{eq:pdxab}
      \lim_{\iix\to0}\frac{f(a+\iix,b) - f(a,b)}{\iix}
    \end{equation}
    存在，则称此极限为函数 $f$ 在点 $(a,b)$ 处对 $x$ 的\textbf{偏导数}，
    否则称函数 $f$ 在 $(a,b)$ 处对 $x$ 不可偏导或偏导数不存在。
  \end{definition}
  
  \onslide<+->
  类似地，可以定义 $f$ 在 $(a,b)$ 处对 $y$ 的偏导数为极限
  \begin{equation}\label{eq:pdyab}
    \lim_{\iiy\to0}\frac{f(a,b+\iiy) - f(a,b)}{\iiy}
  \end{equation}

\end{frame}

\begin{frame}{偏导数的记法}
  \onslide<+->
  \noindent\textbf{变量记法}\quad 函数写成 $z=f(x,y)$, 偏导数可记为
  \onslide<+->
  \begin{align*}
    & \pdx{z}\Bigg|_{\substack{x=a\\y=b}} &
    & \pdx{z}\bigg|_{(a,b)} &
    & \pdx{z}(a,b) &
    & z_x(a,b)
  \end{align*}
  
  \onslide<+->
  \noindent\textbf{函数记法}\quad 函数写成 $f(x,y)$, 偏导数可记为
  \onslide<+->
  \begin{align*}
    &\pdx{f}\bigg|_{\substack{x=a\\y=b}}  &
    &\pdx{f}\bigg|_{(a,b)}  &
    &\pdx{f}(a,b)  &
    &f_x(a,b)
  \end{align*}

  \onslide<+->
  \noindent\textbf{通用记法}\quad 只有函数表达式，如 $x^2+4y^2+3xy^2$, 偏导数可记为
  \onslide<+->
  \[ \pdx\left(x^2+4y^2+3xy^2\right) \]

  \onslide<+->
  \noindent\textbf{抽象记法}\quad 只知道函数名 $f$, \alert{对第一个分量的偏导数}可记为
  \onslide<+->
  \[ f_1(a,b) \]
\end{frame}


\begin{frame}{偏导函数}
  \onslide<+->
  如果函数 $z=f(x,y)$ 在点 $(a,b)$ 处的两个偏导数都存在，
  则称函数在点 $(a,b)$ 处\textbf{可偏导}。

  \onslide<+->
  如果函数 $z=f(x,y)$ 在定义域开区域 $D$ 上的每一点处都可偏导，
  则对于 $D$ 上的任意一点 $(x,y)$ 都有函数 $z=f(x,y)$ 的偏导数与之对应，
  \onslide<+->
  从而可定义函数的\textbf{偏导函数}，记为：
  \begin{align*}
    & z_x && \pdx{z} && f_x && \pdx{f} && f_1\\
    & z_y && \pdy{z} && f_y && \pdy{f} && f_2
  \end{align*}

  \onslide<+->
  不难看出，偏导函数 $f_x$ 在点 $(a,b)$ 处的取值 $f_x(a,b)$ 
  即为函数 $f(x,y)$ 在点 $(a,b)$ 处对 $x$ 的偏导数。
\end{frame}


\begin{frame}{偏导数的代数和几何意义}
  \onslide<+->
  二元函数偏导数的代数和几何意义：
  \begin{enumerate}[<+->]
    \item $\pdx{f}(a,b)$ 表示在点 $(a,b)$ 处当 $y=b$ 保持不变
      时 $x$ 发生变化时函数值 $f(x,y)$ 的变化率；
    \item $\pdx{f}(a,b)$ 表示平面曲线 $\begin{dcases}z=f(x,y),\\y=b.\end{dcases}$ 
      在点 $(a,b,f(a,b))$ 处切线上 $\frac\iiz\iix$ 的值。
      \onslide<+->
      对应的切线的方程为:
      \[ \begin{dcases} y=b\\ z-f(a,b)=\pdx{f}(a,b)(x-a). \end{dcases} \]
  \end{enumerate}
\end{frame}

\subsection{偏导数的计算}
\begin{frame}{用定义计算偏导数}
  \onslide<+->
  \begin{question}
  求函数 $f(x,y)=\begin{dcases}1 &xy=0\\0&xy\neq0\end{dcases}$  
    在点 $(0,0)$ 处的偏导数。
  \end{question}
  \onslide<+->
  \begin{solution}
    由定义可知
    \onslide<+->
    \begin{align*} 
      \onslide<+->{
        f_x(0,0)
        \onslide<+->{ &= \lim_{\iix\to0}\frac{f(\iix,0)-f(0,0)}{\iix} }
        \onslide<+->{ = \lim_{\iix\to0}\frac{0}{\iix} }
        \onslide<+->{ = 0 }\\
      }
      \onslide<+->{
        f_y(0,0) 
        \onslide<+->{ &= \lim_{\iiy\to0}\frac{f(0,\iiy)-f(0,0)}{\iiy} }
        \onslide<+->{ = \lim_{\iiy\to0}\frac{0}{\iiy} }
        \onslide<+->{ = 0 }
      }
    \end{align*}
    \onslide<+->
    即函数 $f(x,y)$ 在 $(0,0)$ 点处可偏导。 
  \end{solution}
  \onslide<+->
  从函数 $f(x,y)$ 的图像上不难看出它在 $(0,0)$ 点处不连续。
\end{frame}


\begin{frame}{可偏导与连续的关系}
  \onslide<+->
  不难看出，函数在点 $(a,b)$ 处可偏导只跟函数在
  直线 $x=a$ 和 $y=b$ 上的取值有关，而与邻域内其它点处的取值无关。
  \onslide<+->
  \begin{ctikzpicture}
    \def\r{2}
    \fill[c1!40] (0,0) circle (\r);
    \fill[c1] (0,0)  node[below left]{$(a,b)$} circle (2pt);
    \draw[c2, very thick](-\r,0) -- (\r,0) node[above, pos=0.75] {$\pdx{f}$};  
    \draw[c3, very thick](0,-\r) -- (0,\r) node[left, pos=0.75] {$\pdy{f}$};
    \onslide<+->{
      \begin{scope}[shift={(5,0)}]
        \fill[c1!50, opacity=0.5] (0,0.5) circle(1.5);
        \draw[c1] (0,1.3) node {可偏导};
        \fill[c2!50, opacity=0.5] (0,-0.5) circle(1.5);
        \draw[c2] (0,-1.3) node {连续};
      \end{scope}
    }
  \end{ctikzpicture}
  \onslide<+->
  即可偏导与连续的关系是：(1) 可偏导不一定连续，(2) 连续不一定可偏导。
\end{frame}


\begin{frame}{用定义计算偏导数}
  \onslide<+->
  \begin{question}
    求函数 $f(x,y)=x^2 + xy$ 在点 $(1,1)$ 处的偏导数。
  \end{question}
  \onslide<+->
  \begin{solution}
    \onslide<+->
    由函数 $f(x,y)$ 的定义可知 
    \[ f(x,1)=x^2+x \]
    \onslide<+->
    对 $x$ 求导可得 
    \[ \ddx{f(x,1)} = 2x + 1 \]
    \onslide<+->
    从而 
    \[ f_x(1,1) \onslide<+->= \big[2x+1\big]_{x=1} \onslide<+->= 3. \]
    \onslide<+->
    同理可得：
    \[ f(1,y)=y+1\qquad \ddy{f(1,y)}=1 \qquad f_y(1,1) = 1. \]
  \end{solution}
\end{frame}


\begin{frame}{偏导数的计算方法}
  \onslide<+->
  由上面的计算方法可知，计算函数的偏导数不需要什么新的方法，
  只需要把其中一个变量看成常数，对另外一个变量求导数即可，
  \onslide<+->用 $f_x(x,y)$ 来说就是\onslide<+->
  
  \[
    f(x,y) 
    \onslide<+-> \xrightarrow[y\text{写成}c]{\quad1\quad} f(x,c) 
    \onslide<+-> \xrightarrow[\text{求导}]{\quad2\quad} \ddx{f(x,c)} 
    \onslide<+-> \xrightarrow[c\text{写成}y]{\quad3\quad} \left.\ddx{f(x,c)}\right|_{c=y}
  \]
  
  \addtocounter{beamerpauses}{-3}
  \begin{enumerate}
    \item\onslide<+-> 把 $f(x,y)$ 中的 $y$ 写成常数 $c$, 此时可写为 $f(x,c)$.
    \item\onslide<+-> 把 $f(x,c)$ 看成关于 $x$ 的一元函数，对其进行求导 $\ddx{f(x,c)}$.
    \item\onslide<+-> 把上面求导结果中的 $c$ 换回 $y$ 即得偏导数 $f_x(x,y)$.
  \end{enumerate}
\end{frame}


\begin{frame}{偏导计算练习}
  \onslide<+->
  \begin{question}
    求函数 $f(x,y)=x^y$ 的偏导数。
  \end{question}
  \onslide<+->
  \begin{solution}
    \onslide<+->
    按照上面的步骤计算 $f_x(x,y)$ 可得
    \begin{enumerate}[<+->]
      \item 把 $f(x,y)$ 中的 $y$ 写成常数 $c$, 即 $f(x,c)=x^c$.
      \item 一元函数 $f(x,c)$ 对 $x$ 的求导，即 $\ddx{x^c}=cx^{c-1}$.
      \item 把求导结果中 $c$ 换回 $y$ 即得偏导数 $f_x(x,y)=y x^{y-1}$.
    \end{enumerate}
    \onslide<+->
    计算 $f_y(x,y)$ 可得
    \begin{enumerate}[<+->]
      \item 把 $f(x,y)$ 中的 $x$ 写成常数 $c$, 即 $f(c,y)=c^y$.
      \item 一元函数 $f(c,y)$ 对 $y$ 的求导，即 $\ddy{c^y}=c^y\ln c$.
      \item 把求导结果中 $c$ 换回 $x$ 即得偏导数 $f_y(x,y)=x^y\ln x$.
    \end{enumerate}
  \end{solution}
\end{frame}


\begin{frame}{偏导计算练习}
  \onslide<+->
  \begin{question}
    求函数 $f(x,y)=x^2+y^2-xy$ 的偏导数。
  \end{question}
  \onslide<+->
  \begin{solution}计算可得
    \begin{align*}
      f_x(x,y) &= 2x - y & 
      f_y(x,y) &= 2y - x
    \end{align*}
  \end{solution}

  \onslide<+->
  \begin{question}
    求函数 $f(x,y)=\sin(x+y) + \ln(xy)$ 的偏导数。
  \end{question}
  \onslide<+->
  \begin{solution}计算可得
    \begin{align*}
      f_x(x,y) &= \cos(x+y) + \frac{y}{xy} = \cos(x+y) + \frac1x \\ 
      f_y(x,y) &= \cos(x+y) + \frac{x}{xy} = \cos(x+y) + \frac1y
    \end{align*}
  \end{solution}
\end{frame}


\begin{frame}{偏导不是微商}
  \onslide<+->
  \begin{question}
    已知一定量理想气体的状态方程为 $PV=RT$ （其中 $R$ 为常数），
    求偏导数 $\pd{V}{P}$, $\pd{T}{V}$, $\pd{P}{T}$ 及它们的乘积。
  \end{question}
  \onslide<+->
  \begin{solution}\small
    \onslide<+->
    由方程得 $P=\frac{RT}{V}$, 把 $P$ 看成 $T$, $V$ 的函数对 $V$ 求偏导得
    \onslide<+->
    \[ \pd{V}{P}=-\frac{RT}{V^2} \]
    \onslide<+->
    由方程得 $V=\frac{RT}{P}$, 把 $V$ 看成 $T$, $P$ 的函数对 $P$ 求偏导得
    \onslide<+->
    \[ \pd{T}{V}=\frac{R}{P} \]
    \onslide<+->
    由方程得 $T=\frac{PV}{R}$, 把 $T$ 看成 $P$, $V$ 的函数对 $P$ 求偏导得
    \onslide<+->
    \[ \pd{P}{T}=\frac{V}{R} \]
    \onslide<+->
    从而
    \[ 
      \pd{V}{P} \cdot \pd{T}{V} \cdot \pd{P}{T} 
      \onslide<+-> = -\frac{RT}{V^2} \cdot \frac{R}{P} \cdot \frac{V}{R}
      \onslide<+-> = -\frac{RT}{PV}
      \onslide<+-> = -1
    \]
  \end{solution}
  %从这个例子中可以看出，和一元函数的导数 $\ddx{y}$ 可以看成微分 $\dy$ 与 $\dx$ 的商
  %不同，偏导数的记法 $\pd{x}{z}$ 是一个整体符号，不能看成是商的结果。
\end{frame}

\section{高阶偏导}
\begin{frame}{二阶偏导}
  \onslide<+->
  二元函数的偏导数还是二元函数，所以也可以对它求偏导，称为原来函数的\textbf{二阶偏导数}。
  \onslide<+->
  定义和记法为：
  \begin{align*}
    \pdxx{f} &= \pdx\left(\pdx{f}\right) &
    \pdyx{f} &= \pdy\left(\pdx{f}\right) \\
    \pdxy{f} &= \pdx\left(\pdy{f}\right) &
    \pdyy{f} &= \pdy\left(\pdy{f}\right) 
  \end{align*}
  \onslide<+->
  \begin{remark}
    为了保证符号的一致性和简单性，这里混合偏导的记法和书上有些区别。
  \end{remark}
  \onslide<+->
  上面四个二阶偏导数还可以依次记为
  \begin{align*}
    &f_{xx} && f_{xy} && f_{yx} && f_{yy} \\
    &f_{11} && f_{12} && f_{21} && f_{22} 
  \end{align*}
\end{frame}


\begin{frame}{二阶偏导练习}
  \onslide<+->
  \begin{question}
    求函数 $f(x,y)=x^3y-3x^2y^3$ 的所有二阶偏导数。
  \end{question}
  \onslide<+->
  \begin{solution}
    \onslide<+->
    计算函数的一阶偏导数可得：
    \begin{align*}
      f_x(x,y) &= 3x^2y-6xy^3 &
      f_y(x,y) &= x^3-9x^2y^2
    \end{align*}
    \onslide<+->
    再求二阶偏导数可得：
    \begin{align*}
      f_{xx}(x,y) &= 6xy-6y^3 &
      f_{yx}(x,y) &= 3x^2-18xy^2 \\
      f_{xy}(x,y) &= 3x^2-18xy^2 &
      f_{yy}(x,y) &= -18x^2y 
    \end{align*}
  \end{solution}
\end{frame}




\begin{frame}{二阶偏导数的对称性}
  \onslide<+->
  \begin{theorem}[混合偏导交换次序]
    设函数 $f(x,y)$ 的两个混合偏导数 $\pdyx{f}$ 
    和 $\pdxy{f}$ 在开区域 $D$ 内连续，则:
    \[ \pdyx{f} = \pdxy{f} \]
  \end{theorem}

  \onslide<+->
  这个定理可以推广到高阶的混合偏导中，如
  \[ \pd[3]{x^2,y}{f} = \pd[3]{x,y,x}{f} = \pd[3]{y,x^2}{f} \]
  当这三个偏导数都连续时。
\end{frame}


\begin{frame}{混合偏导存在但不相等的例子}
    \[
      f(x,y) = 
      \begin{dcases} 
        \frac{xy(x^2-y^2)}{x^2+y^2},& (x,y)\neq(0,0) \\
        0, & (x,y)=(0,0).
      \end{dcases} 
    \]
\end{frame}



\begin{frame}{调和函数}
  \onslide<+->
  \begin{question}
    验证函数 $f(x,y)=\ln(x^2+y^2)$ 
    满足 $\pdxx{f} + \pdyy{f} = 0 $.
  \end{question}
  \onslide<+->
  \begin{solution}
    \onslide<+->
    求函数的一阶偏导数可得：
    \begin{align*}
      f_x &= \frac{2x}{x^2+y^2} &
      f_y &= \frac{2y}{x^2+y^2}
    \end{align*}
    \onslide<+->
    再求函数的二阶偏导数可得：
    \begin{align*}
      f_{xx} 
      &= \frac{2}{x^2+y^2} - \frac{4x^2}{\left(x^2+y^2\right)^2} 
      = \frac{2y^2-2x^2}{\left(x^2+y^2\right)^2} \\
      f_{yy}
      &= \frac{2}{x^2+y^2} - \frac{4y^2}{\left(x^2+y^2\right)^2} 
      = \frac{2x^2-2y^2}{\left(x^2+y^2\right)^2}
    \end{align*}
    \onslide<+->
    从而
    \[
      f_{xx} + f_{yy} 
      = \frac{2y^2-2x^2}{\left(x^2+y^2\right)^2} + 
        \frac{2x^2-2y^2}{\left(x^2+y^2\right)^2}
      = 0
    \]
  \end{solution}
\end{frame}

\begin{frame}{调和函数}
  \onslide<+->
  \begin{question}
    验证函数 $f(x,y,z)=\frac1{\sqrt{x^2+y^2+z^2}}$ 满足拉普拉斯方程 
    \[ \pdxx{f} + \pdyy{f} + \pdzz{f} = 0 \]
  \end{question}
  \onslide<+->
  \begin{solution}
    \onslide<+->
    记 $r=\sqrt{x^2+y^2+z^2}$, 求函数的一阶偏导数可得：
    \begin{align*}
      f_x &= \frac{-x}{r^3} &
      f_y &= \frac{-y}{r^3} &
      f_z &= \frac{-z}{r^3}
    \end{align*}
    \onslide<+->
    再求函数的二阶偏导数可得：
    \begin{align*}
      f_{xx} &= \frac{-1}{r^3} + \frac{3x^2}{r^5} &
      f_{yy} &= \frac{-1}{r^3} + \frac{3y^2}{r^5} &
      f_{zz} &= \frac{-1}{r^3} + \frac{3z^2}{r^5}
    \end{align*}
    \onslide<+->
    从而
    \[
      f_{xx} + f_{yy} + f_{zz}
      = \frac{-3}{r^3} + \frac{3(x^2+y^2+z^2)}{r^5}
      = 0
    \]
  \end{solution}
\end{frame}
\end{document}
