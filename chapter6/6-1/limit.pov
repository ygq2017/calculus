// POV-Ray 3.7 Scene File
//----------------------------------------------------------------------
#version 3.7;
global_settings { 
  assumed_gamma 1.0 
  charset utf8  
}
#default{ 
  finish{ ambient 0.1 diffuse 0.9 }
} 
#include "colors.inc"    
//----------------------------------------------------------------------
camera {      // 
  angle     20
  right     -x*image_width/image_height
  sky       <0,0,1>                     // right handed!!!!!!!!
  location  <7,-6,3>  
  look_at   <0, 0, 0>
}

light_source { <100,-100,100> color White }
#default { finish {phong 0.5 phong_size 10} }

// 地面
plane { <0,0,1> , -1.3 pigment { checker Gray White } }


union{ 
  isosurface {
    function { (x*y)/(x*x+y*y) - z }
    open
    contained_by { box {-1*<1,1,1.1>, <1,1,1.1>} }
    accuracy 0.00001
    max_gradient 200
    pigment { 
      gradient z 
      pigment_map {
        [0.00 Yellow]
        [1.00 Red]
      }
      scale<1,1,2>
      translate<0,0,-1>
    }
  }
  no_shadow
}
