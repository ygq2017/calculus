#include "math.inc"   
#include "colors.inc"
#include "shapes3.inc"
#include "analytical.inc"


camera {
  right  -x*image_width/image_height
  sky    <0,0,1>
  location <4,1.5,3>*1.1
  look_at  <0,0.5,0.8>
}


sky_sphere { pigment {
    function{abs(z)}
    color_map { 
      [0.0   color Magenta]
      [0.33  color Yellow]
      [0.67  color Cyan]
      [1.0   color Magenta]
    }
  }
}

light_source { <0,0,100.0> color White parallel point_at<0,0,0>}
#default { finish {phong 0.5 phong_size 10} }

// 地面
plane { <0,0,1> , -3 pigment { checker color White, color Gray } }

#declare T_0 = texture { pigment { White } }
#declare T_1 = texture { pigment { Blue } }
#declare R = 3; // 圆柱半径

#declare value = 1-2*clock;

union{ 
  // 坐标轴
  AxisXYZ(<1.5,3.5,2.5>, 0.03, T_0, T_1)
  text { ttf "timrom.ttf", "x", 0.1, 0  rotate <90,0,90>  translate 1.5*x pigment { Blue } }
  text { ttf "timrom.ttf", "y", 0.1, 0  rotate <90,0,180> translate 3.5*y pigment { Blue } } 
  text { ttf "timrom.ttf", "z", 0.1, 0  rotate <90,0,90>  translate 2.5*z pigment { Blue } } 
  // 椭球体 
  intersection {
    sphere { 0, 1 scale <1,3,2> pigment { Yellow } }
    plane { x, value pigment { Red } }
  }
  //MyObject
  no_shadow
}
