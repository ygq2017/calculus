#include "math.inc"   
#include "colors.inc"
#include "shapes3.inc"
#include "analytical.inc"


camera {
  right  -x*image_width/image_height
  sky    <0,0,1>
  location <5,2,5>
  look_at  <0,0.5,0.8>
}

sky_sphere { pigment {
    function{abs(z)}
    color_map { 
      [0.0   color Magenta]
      [0.33  color Yellow]
      [0.67  color Cyan]
      [1.0   color Magenta]
    }
  }
}

light_source { <100,0,100> color White }
#default { finish {phong 0.5 phong_size 10} }

// 地面
plane { <0,0,1> , -3 pigment { checker color White, color Gray } }

#declare T_0 = texture { pigment { White } }
#declare T_1 = texture { pigment { Blue } }
#declare R = 3; // 圆柱半径

#declare value = 2*(1-2*clock);

union{ 
  // 坐标轴
  AxisXYZ(<2.5,3.5,3.5>, 0.03, T_0, T_1)
  text { ttf "timrom.ttf", "x", 0.1, 0  rotate <90,0,90>  translate 2.5*x pigment { Blue } }
  text { ttf "timrom.ttf", "y", 0.1, 0  rotate <90,0,180> translate 3.5*y pigment { Blue } } 
  text { ttf "timrom.ttf", "z", 0.1, 0  rotate <90,0,90>  translate 3.5*z pigment { Blue } } 

  sphere {
    0, 1 scale <1,3,2>
    pigment { Yellow transmit 0.2*clock }
  }
  no_shadow
}
