#include "math.inc"   
#include "colors.inc"
#include "shapes3.inc"
#include "analytical.inc"

#declare CamLoc = <8,3,4>;
camera {
  right  -x*image_width/image_height
  sky    <0,0,1>
  location CamLoc
  look_at  <0,0,0>
}


sky_sphere { pigment {
    function{abs(z)}
    color_map { [0.0 color blue 0.6] [1.0 color rgb 1] }
  }
}

light_source { <100,0.0,100.0> color White}
#default { finish {phong 0.5 phong_size 10} }

// 地面
plane { <0,0,1> , -5 pigment { checker color White, color Gray } }

#declare T_0 = texture { pigment { White } }
#declare T_1 = texture { pigment { Red } }
union{ 
  // 坐标轴
  AxisXYZ(<4,4,4>, 0.05, T_0, T_1)
  text { ttf "timrom.ttf", "x", 0.1, 0 Reorient_Trans(z, CamLoc) translate 4*x pigment { Blue } }
  text { ttf "timrom.ttf", "y", 0.1, 0 Reorient_Trans(z, CamLoc) translate 4*y pigment { Blue } } 
  text { ttf "timrom.ttf", "z", 0.1, 0 Reorient_Trans(z, CamLoc) translate 4*z pigment { Blue } }
  no_shadow
}
// 旋转后的图形


#declare Rcylinder = 2; // 圆柱半径
#declare Rsphere = 2; // 球面半径
// 球心 Rcenter: 0, Rcylinder + Rsphere
#declare Rcenter = (Rcylinder+Rsphere)*(1-clock)*x ;  
#declare Delta = 0.02;


// 球面与球心
sphere {
  Rcenter, Rsphere
  pigment { Yellow transmit 0.5 }
  no_shadow
}
sphere { Rcenter, 0.1 pigment { Yellow } no_shadow }

// 柱面
cylinder {
  -(Rsphere+0.5)*z, (Rsphere+0.5)*z, Rcylinder
  open
  pigment { Cyan transmit 0.5 }
  no_shadow
}

// 画交线


object {
  intersection {
    difference {
      cylinder { -Rsphere*z, Rsphere*z, Rcylinder+Delta }
      cylinder { -Rsphere*z, Rsphere*z, Rcylinder-Delta }
    }
    difference {
      sphere { Rcenter, Rsphere+Delta }
      sphere { Rcenter, Rsphere-Delta }
    }
  }
  pigment { Red }
  no_shadow
}
