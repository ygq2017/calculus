// POV-Ray 3.7 Scene File
// Right Handed Coordinate System
//----------------------------------------------------------------------
#version 3.7;
global_settings { 
  assumed_gamma 1.0 
  charset utf8  
}
#default{ 
  finish{ ambient 0.1 diffuse 0.9 }
} 
//----------------------------------------------------------------------
// stdinc: colors, consts, functions, math, rand, shapes, transforms 
#include "stdinc.inc"    
#include "shapes2.inc"
#include "shapes3.inc"   // shapes, shapes_old transforms, math, strings,
#include "textures.inc"  // also: finish
#include "analytical.inc"
//----------------------------------------------------------------------
camera {      // 
  angle     18
  right     -x*image_width/image_height
  sky       <0,0,1>                     // right handed!!!!!!!!
  location  25*<1,1,1>
  look_at   <0, 0, 0>
  rotate   <0,0,360*clock>
}
//----------------------------------------------------------------------
sky_sphere { pigment {
    function{abs(z)}
    color_map { 
      [0.0   color Magenta]
      [0.33  color Yellow]
      [0.67  color Cyan]
      [1.0   color Magenta]
    }
  }
}

light_source { <100,0,100> color White }
#default { finish {phong 0.5 phong_size 10} }

// 地面
plane { <0,0,1> , -3 pigment { checker color White, color Gray } }

#declare T0 = texture { pigment { White } }
#declare Tx = texture { pigment { Red } }
#declare Ty = texture { pigment { Green } }
#declare Tz = texture { pigment { Blue } }
#declare R = 3; // 圆柱半径

union{ 
  // 坐标轴
  AxisXYZ(-5*<1,1,1>, 5*<1,1,1>, 0.05, T0, Tx, Ty, Tz)
  
  isosurface {
    function { x*x - y*y + z }
    open
    contained_by { sphere { 0, 3 } }
    accuracy 0.0001
    max_gradient 20
    pigment { Yellow }
  }
  no_shadow
}

