#!/bin/bash
fname='hyperbolic-paraboloid'

echo "; POV-Ray setting file
Input_File_Name=$fname.pov

Display=Off
Verbose=Off

[720p]
Width=1280
Height=720

Output_to_File=true
Output_File_Type=N16
Quality=11

Antialias=On
Antialias_Threshold=0.1 ; best: 0.0

Sampling_Method=2

Antialias_Depth=3 ; best: 9

Jitter=On
Jitter_Amount=0.3 ; best: 1.0

Initial_Frame=0
Final_Frame=360
Initial_Clock=0
Final_Clock=1

Cyclic_Animation=on
Pause_when_Done=off
" > $fname.ini

#povray $fname

#ffmpeg -i $fname-%03d.png \
#       -framerate 60 \
#       -c:v libx264 -pix_fmt yuv420p \
#       $fname.mp4
