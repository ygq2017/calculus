#ifndef(ANALYTICAL_INC_TEMP)
#declare ANALYTICAL_INC_TEMP = version;
#version 3.7;

#ifdef(View_POV_Include_Stack)
   #debug "including analytical.inc\n"
#end 

#include "math.inc"
#include "consts.inc"
#include "transforms.inc"
#include "functions.inc"

#declare Golden_Ratio = (1+sqrt(5))/2;     // 常数黄金比例

// 向量对象
#macro Vec (V, Rl)
  #local Len = vlength(V);
  #local Dir = V/Len;
  #if ( Len >=(1+Golden_Ratio)*Golden_Ratio*Golden_Ratio*Rl )
    #local Cone_Start = Len - (1+Golden_Ratio)*Golden_Ratio*Golden_Ratio*Rl;
    #local Cylinder_Len = Cone_Start + 0.2*Rl;
  #else
    #local Cone_Start = Len*Golden_Ratio/(1+Golden_Ratio);
    #local Cylinder_Len = Cone_Start + Len*0.2/(1+Golden_Ratio);
  #end
  
  union {
    cylinder { o, Cylinder_Len*Dir, Rl }
    cone { Cone_Start*Dir, 1.618*Rl, Len*Dir, 0 }
  }
#end


// 纯色向量
#macro Vec_PP ( P_Start, P_End, Rl )
  object {
    Vec(P_End-P_Start, Rl)
    translate P_Start
  }
#end




////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
/* 坐标轴的宏基础
   dir: the direction of axis
*/ 
#macro Axis_(Dir, Min, Max, Rl, Texture_0, Texture_1)
  #if (Min!=Max)
    object {
      Vec_PP(Min*x, Max*x, Rl)
      texture {
        checker 
          texture{ Texture_0 } 
          texture{ Texture_1 }
          translate 0.5*yz
      }
      Reorient_Trans(x, Dir)
    }
  #end
#end

#macro AxisX(Min, Max, Rl, Texture_0, Texture_1)
  Axis_(x, Min, Max, Rl, Texture_0, Texture_1)
#end
#macro AxisY(Min, Max, Rl, Texture_0, Texture_1)
  Axis_(y, Min, Max, Rl, Texture_0, Texture_1)
#end
#macro AxisZ(Min, Max, Rl, Texture_0, Texture_1)
  Axis_(z, Min, Max, Rl, Texture_0, Texture_1)
#end

#macro AxisXYZ(Min, Max, Rl, Texture_0, Texture_x, Texture_y, Texture_z)
  union {
    AxisX(Min.x, Max.x, Rl, Texture_x, Texture_0)
    AxisY(Min.y, Max.y, Rl, Texture_y, Texture_0)
    AxisZ(Min.z, Max.z, Rl, Texture_z, Texture_0)
  }
#end



#version ANALYTICAL_INC_TEMP;
#end
