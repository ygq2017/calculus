#include "base.pov"


isosurface {
    function { abs(3*(x*x+y*y) - z*z) - 0.1 }
    contained_by { box { <-2,-2,0>, 2 } }
    max_gradient 18
    accuracy 0.0001
    pigment { Yellow transmit 0.3 } 
}
