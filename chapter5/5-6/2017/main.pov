#include "base.pov"    


union{ // 圆柱面 
  isosurface {
    function { abs(x*x + y*y - 1)-0.01 }
    open
    contained_by { box { -3, 3 } }
    accuracy 0.0001
    max_gradient 20
    pigment { Yellow }
  }
  no_shadow
}

