#include "math.inc"   
#include "colors.inc"
#include "shapes3.inc"
#include "analytical.inc"

#declare CamLoc = <12,0,6>;
camera {
  right  -x*image_width/image_height
  sky    <0,0,1>
  location CamLoc
  look_at  <0,0,0>
}


sky_sphere { pigment {
    function{abs(z)}
    color_map { 
      [0.0   color Magenta]
      [0.33  color Yellow]
      [0.67  color Cyan]
      [1.0   color Magenta]
    }
  }
}

light_source { <100,0.0,100.0> color White}
#default { finish {phong 0.5 phong_size 10} }

// 地面
plane { <0,0,1> , -5 pigment { checker color White, color Gray } }

#declare T_0 = texture { pigment { White } }
#declare T_1 = texture { pigment { Blue } }
#declare R = 3; // 圆柱半径
//plane: z=Kx
#declare K = -1*clock;

union{ 
  // 坐标轴
  AxisXYZ(<4.5,4.5,4.5>, 0.03, T_0, T_1)
  text { ttf "timrom.ttf", "x", 0.1, 0  rotate <90,0,90>  translate 4.5*x pigment { Blue } }
  text { ttf "timrom.ttf", "y", 0.1, 0  rotate <90,0,180> translate 4.5*y pigment { Blue } } 
  text { ttf "timrom.ttf", "z", 0.1, 0  rotate <90,0,90>  translate 4.5*z pigment { Blue } }

  difference {
    // 柱面
    cylinder { -4*z, 4*z, R pigment { Cyan } }
  
    // 平面
    plane { <K,0,-1>, 0 pigment { Yellow } }
  }
  // 画交线
  #local phi = 0;
  #while (phi<2*pi)
    sphere { <R*cos(phi), R*sin(phi), K*R*cos(phi)>, 0.05 pigment { Red } }
    #local phi = phi + 0.005;
  #end
  
  rotate -20*z
  no_shadow
}
