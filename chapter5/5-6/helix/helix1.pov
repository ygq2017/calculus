#include "math.inc"   
#include "colors.inc"
#include "shapes3.inc"
#include "analytical.inc"

camera {
  right  -x*image_width/image_height
  sky    <0,0,1>
  location <8,0,10>
  look_at  <0,0,4>
}


sky_sphere { pigment {
    function{abs(z)}
    color_map { 
      [0.0   color Magenta]
      [0.33  color Yellow]
      [0.67  color Cyan]
      [1.0   color Magenta]
    }
  }
}

light_source { <100,0.0,100.0> color White}
#default { finish {phong 0.5 phong_size 10} }

// 地面
plane { <0,0,1> , -0.2 pigment { checker color White, color Gray } }

#declare T_0 = texture { pigment { White } }
#declare T_1 = texture { pigment { Blue } }
#declare R = 2; // 圆柱半径
#declare MaxAngle = 21*pi*clock;

union{ 
  // 坐标轴
  AxisXYZ(<3.0,4.5,8.0>, 0.03, T_0, T_1)
  text { ttf "timrom.ttf", "x", 0.1, 0  rotate <90,0,90>  translate 3.0*x pigment { Blue } }
  text { ttf "timrom.ttf", "y", 0.1, 0  rotate <90,0,180> translate 4.5*y pigment { Blue } } 
  text { ttf "timrom.ttf", "z", 0.1, 0  rotate <90,0,90>  translate 8.0*z pigment { Blue } }
  // 螺线
  union {
    #local phi = 0;
    #while (phi<MaxAngle)
      #declare per = phi/(21*pi);
      sphere { <3*per*cos(phi), 3*per*sin(phi), 8*(1-per)>, 0.05 pigment { Red } }
      #local phi = phi + 0.005;
    #end
    #declare per = MaxAngle/(21*pi);
    sphere { <3*per*cos(phi), 3*per*sin(phi), 8*(1-per)>, 0.1 pigment { Cyan } }
  }
  rotate -20*z
  no_shadow
}
