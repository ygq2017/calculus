# 王二民的微积分课程网站

我在郑州工业应用技术学院教的微积分课程的相关教学资料。

点击上面的 [Repository](https://gitlab.com/wagermn-course/calculus/tree/master) 标签或[这里](https://gitlab.com/wagermn-course/calculus/tree/master)以查看课程资料。

## 教学辅助软件 

 * [Desmos 图形计算器](https://www.desmos.com/calculator)
 * [Wolfram Alpha 计算搜索引擎](https://www.wolframalpha.com)
 * [Geogebra 数学应用](https://www.geogebra.org/apps)
 * [Octave 在线数学软件](https://octave-online.net)
 * [SageMath 开源数学软件](https://cloud.sagemath.com)
 * [Symbolab 一步一步的计算器](https://www.symbolab.com)
